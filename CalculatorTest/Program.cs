﻿using CalculatorTest.Calculation;
using System;

namespace CalculatorTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("No input.");
                return;
            }

            string userInput = args[0];

            try
            {
                Console.WriteLine("Problem: " + userInput);

                ProblemSolver problem = new ProblemSolver(userInput);

                if (problem.IsSolved)
                {
                    Console.WriteLine("Solution: " + problem.Solution.ToString());
                }
                else
                {
                    Console.WriteLine("Unable to solve the provided problem.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("A fatal error occured while attempting to solve the provided problem.");
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
