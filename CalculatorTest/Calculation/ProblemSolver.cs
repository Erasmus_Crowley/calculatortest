﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTest.Calculation
{
    public class ProblemSolver
    {
        public decimal Solution { get; }

        private List<Segment> _segments;

        #region Read-only properties

        public bool IsSolved
        {
            get
            {
                return _segments.Count == 1 && _segments[0].IsNumeric;
            }
        }

        private bool CanMultiply
        {
            get
            {
                return this._segments
                    .Where(s => s.IsOperator && s.Operation == OperationType.Multiply)
                    .Any();
            }
        }

        private bool CanDivide
        {
            get
            {
                return this._segments
                    .Where(s => s.IsOperator && s.Operation == OperationType.Divide)
                    .Any();
            }
        }

        private bool CanAddOrSubtract
        {
            get
            {
                return this._segments
                    .Where(s => s.IsOperator && (s.Operation == OperationType.Add || s.Operation == OperationType.Subtract))
                    .Any();
            }
        }

        #endregion

        public ProblemSolver(string input)
        {
            this._segments = ProblemParser.Parse(input);

            //FOR DEBUGGING
            //PrintSegmentsToConsole();

            DoMultiplicationPass();
            DoDivisionPass();
            DoAdditionAndSubtractionPass();

            if (IsSolved)
            {
                this.Solution = this._segments[0].Value;
            }
        }

        private void PrintSegmentsToConsole()
        {
            Console.WriteLine("Printing calculation segments:");

            foreach (Segment s in _segments)
            {
                Console.WriteLine(s.Contents);
            }
        }

        private void DoMultiplicationPass()
        {
            while (CanMultiply)
            {
                Segment op = FindNextMultiplyOperator();

                int index = this._segments.IndexOf(op);

                Segment left = this._segments[index - 1];
                Segment right = this._segments[index + 1];

                Segment result = new Segment(left.Value * right.Value);

                this._segments.Insert(index, result);

                this._segments.Remove(left);
                this._segments.Remove(op);
                this._segments.Remove(right);
            }
        }

        private void DoDivisionPass()
        {
            while (CanDivide)
            {
                Segment op = FindNextDivideOperator();

                int index = this._segments.IndexOf(op);

                Segment left = this._segments[index - 1];
                Segment right = this._segments[index + 1];

                Segment result = new Segment(left.Value / right.Value);

                this._segments.Insert(index, result);

                this._segments.Remove(left);
                this._segments.Remove(op);
                this._segments.Remove(right);
            }
        }

        private void DoAdditionAndSubtractionPass()
        {
            while(CanAddOrSubtract)
            {
                Segment op = FindNextAddOrSubtractOperator();

                int index = this._segments.IndexOf(op);

                Segment left = this._segments[index - 1];
                Segment right = this._segments[index + 1];
                Segment result = null;

                if (op.Operation == OperationType.Add)
                {
                    result = new Segment(left.Value + right.Value);
                }

                if (op.Operation == OperationType.Subtract)
                {
                    result = new Segment(left.Value - right.Value);
                }

                this._segments.Insert(index, result);

                this._segments.Remove(left);
                this._segments.Remove(op);
                this._segments.Remove(right);
            }
        }

        #region Operation Find Functions

        private Segment FindNextMultiplyOperator()
        {
            Segment operation = this._segments
                    .Where(s => s.IsOperator && s.Operation == OperationType.Multiply)
                    .FirstOrDefault();

            return operation;
        }

        private Segment FindNextDivideOperator()
        {
            Segment operation = this._segments
                    .Where(s => s.IsOperator && s.Operation == OperationType.Divide)
                    .FirstOrDefault();

            return operation;
        }

        private Segment FindNextAddOrSubtractOperator()
        {
            Segment operation = this._segments
                    .Where(s => s.IsOperator && (s.Operation == OperationType.Add || s.Operation == OperationType.Subtract))
                    .FirstOrDefault();

            return operation;
        }

        #endregion
    }
}
