﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTest.Calculation
{
    public static class Operators
    {
        private static readonly string _whitelist = "+-*/";

        public static bool IsValidOperator(char input)
        {
            foreach (char c in _whitelist)
            {
                if (c == input)
                {
                    return true;
                }
            }

            return false;
        }

        public static OperationType FindOperationType(char input)
        {
            if (input == '+')
                return OperationType.Add;

            if (input == '-')
                return OperationType.Subtract;

            if (input == '*')
                return OperationType.Multiply;

            if (input == '/')
                return OperationType.Divide;

            return OperationType.Invalid;
        }
    }
}
