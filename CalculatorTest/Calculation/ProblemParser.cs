﻿using System.Collections.Generic;
using System.Text;

namespace CalculatorTest.Calculation
{
    public static class ProblemParser
    {
        private static string _whitelist = ".0123456789*/-+";

        private static StringBuilder _sb = new StringBuilder();
        private static List<Segment> _segments = new List<Segment>();

        public static List<Segment> Parse(string input)
        {
            _sb.Clear();
            _segments.Clear();

            string cleanedInput = SanitizeString(input);

            foreach (char c in cleanedInput)
            {
                if (!Operators.IsValidOperator(c))
                {
                    //Assume this character is part of the current value being parsed
                    _sb.Append(c);
                }
                else
                {
                    //This character is an operator type.
                    // assume that this indicates that the value currently held in _sb is completed.
                    if (_sb.Length > 0)
                    {
                        //Cut the string, save the segment.
                        FinishCurrentSegment();

                        //Also add the operator as a new segment.
                        _sb.Append(c);
                        FinishCurrentSegment();
                    }
                }
            }

            //If we reached the end of the string and there is a value in _sb, add it to the segments.
            if (_sb.Length > 0)
            {
                FinishCurrentSegment();
            }

            return _segments;
        }

        private static string SanitizeString(string input)
        {
            StringBuilder cleaned = new StringBuilder();

            foreach(char c in input.Trim())
            {
                if (_whitelist.IndexOf(c) > 0)
                {
                    cleaned.Append(c);
                }
            }

            return cleaned.ToString();
        }

        private static void FinishCurrentSegment()
        {
            Segment s = new Segment(_sb.ToString());
            _segments.Add(s);
            _sb.Clear();
        }
    }
}
