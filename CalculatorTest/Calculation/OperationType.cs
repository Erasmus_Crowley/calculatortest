﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTest.Calculation
{
    public enum OperationType
    {
        Invalid,
        Multiply,
        Divide,
        Add,
        Subtract
    }
}
