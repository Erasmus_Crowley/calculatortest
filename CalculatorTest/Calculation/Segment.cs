﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTest.Calculation
{
    public class Segment
    {
        public string Contents { get; set; }

        private readonly bool _isOperator = false;
        private readonly OperationType _operation = OperationType.Invalid;

        private readonly bool _isNumeric = false;
        private readonly decimal _value = 0;

        #region Read-only Properties

        public bool IsOperator
        {
            get
            {
                return this._isOperator;
            }
        }

        public OperationType Operation
        {
            get
            {
                return this._operation;
            }
        }

        public bool IsNumeric
        {
            get
            {
                return this._isNumeric;
            }
        }

        public decimal Value
        {
            get
            {
                return this._value;
            }
        }

        #endregion

        public Segment(string value)
        {
            this.Contents = value;

            this._operation = Operators.FindOperationType(this.Contents[0]);
            this._isOperator = this._operation != OperationType.Invalid;
            this._isNumeric = decimal.TryParse(value, out _value);
        }

        public Segment(decimal value)
        {
            this.Contents = value.ToString();

            this._operation = OperationType.Invalid;
            this._isOperator = false;
            this._isNumeric = true;
            this._value = value;
        }
    }
}
